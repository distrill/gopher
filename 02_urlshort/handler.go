package urlshort

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gopkg.in/yaml.v2"
)

func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if url, ok := pathsToUrls[r.URL.Path]; ok {
			http.Redirect(w, r, url, http.StatusSeeOther)
		} else {
			fallback.ServeHTTP(w, r)
		}
	})
}

type redirect struct {
	Path string `yaml:"path" json:"path"`
	Url  string `yaml:"url" json:"url"`
}

func YAMLHandler(yml []byte, fallback http.Handler) (http.HandlerFunc, error) {
	redirects := []redirect{}
	err := yaml.Unmarshal(yml, &redirects)
	if err != nil {
		return nil, err
	}

	pathsToUrls := make(map[string]string)
	for _, record := range redirects {
		pathsToUrls[record.Path] = record.Url
	}
	fmt.Println(pathsToUrls)

	return MapHandler(pathsToUrls, fallback), nil
}

func JSONHandler(j []byte, fallback http.Handler) (http.HandlerFunc, error) {
	redirects := []redirect{}
	err := json.Unmarshal(j, &redirects)
	if err != nil {
		return nil, err
	}

	pathsToUrls := make(map[string]string)
	for _, record := range redirects {
		pathsToUrls[record.Path] = record.Url
	}
	fmt.Println(pathsToUrls)

	return MapHandler(pathsToUrls, fallback), nil
}
