package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

func quiz(lines [][]string, correct *int) {
	scanner := bufio.NewScanner(os.Stdin)
	for _, line := range lines {
		// coerce real answer into string, user's stdin will always be a string
		q := line[0]
		a := fmt.Sprintf("%v", line[1])

		fmt.Printf("what is %v?\n", q)
		scanner.Scan()
		text := strings.TrimSpace(scanner.Text())

		if text == a {
			*correct++
		}
	}
	fmt.Printf("You got %v out of %v right\n", *correct, len(lines))
	os.Exit(0)
}

func main() {
	// user input to customize quiz experience
	filename := flag.String("file", "problems.csv", "file to read questions from")
	timeout := flag.Int("timeout", 30, "number of seconds to answer questions")
	shuffle := flag.Bool("shuffle", false, "shuffle question order")
	flag.Parse()

	// parse csv into questions for quiz
	file, err := os.Open(*filename)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}
	r := csv.NewReader(file)
	questions, err := r.ReadAll()
	if err != nil {
		log.Fatalf("failed reading lines: %s", err)
	}

	if *shuffle {
		rand.Seed(time.Now().UnixNano())
		for i := len(questions) - 1; i > 0; i-- {
			j := rand.Intn(i + 1)
			questions[i], questions[j] = questions[j], questions[i]
		}
	}

	// user confirm begin quiz
	fmt.Printf("You will have %v seconds to complete the quiz. Press enter to continue\n", *timeout)
	_, err = bufio.NewReader(os.Stdin).ReadBytes('\n')
	if err != nil {
		log.Fatalf("failed reading input: %s", err)
	}

	// quiz as
	correct := 0
	go quiz(questions, &correct)
	time.Sleep(time.Duration(*timeout) * time.Second)
	fmt.Printf("You got %v out of %v right\n", correct, len(questions))
}
